#!/usr/bin/env python3

import mysql.connector
from mysql.connector import Error
import cherrypy
import config
import html

class VulnerableApp(object):
    def __init__(self):
        self.conn = mysql.connector.connect(host=config.DB_HOST, user=config.DB_USER, database=config.DB_NAME, password=config.DB_PASS)

    @cherrypy.expose
    def index(self, **post):
        chaines = []
        try:
            html.escape("<script>")
            cursor = self.conn.cursor(prepared=True)
            if cherrypy.request.method == "POST":
               
                #requete = "INSERT INTO chaines (txt) VALUES(\"" + post["chaine"] + "\");"
                requete = """ INSERT INTO chaines (txt) VALUES(%s);"""
                    
                valeur = [post["chaine"]]
                for i in valeur :                       
                    if(html.escape("<script>") in i):
                        print(html.escape("<script>") in i)
                    else:
                        print(requete)
                        cursor.execute(requete,valeur)
                        self.conn.commit()
                   
                       
            
        except mysql.connector.Error as error:
            print("parameterized query failed {}".format(error))
        finally:
            if (self.conn.is_connected()):
                cursor.execute("SELECT txt FROM chaines");
                for row in cursor.fetchall():
                    chaines.append(row[0])
                cursor.close()
                print("MySQL connection is closed")
                #self.conn.close()
                
        return '''
<html>
<head>
<title>Application Python Vulnerable</title>
</head>
<body>
<p>
Bonjour, je suis une application vulnerable qui sert a inserer des chaines dans une base de données MySQL!
</p>

<p>
Liste des chaines actuellement insérées:
<ul>
'''+"\n".join(["<li>" + s + "</li>" for s in chaines])+'''
</ul>
</p>

<p> Inserer une chaine:

<form method="post" onsubmit="return validate()">
<input type="text" name="chaine" id="chaine" value="" />
<br />
<input type="submit" name="submit" value="OK" />
</form>

<script>
function validate() {
    var regex = /^[a-zA-Z0-9]+$/;
    var chaine = document.getElementById('chaine').value;
    console.log(regex.test(chaine));
    if (!regex.test(chaine)) {
        alert("Veuillez entrer une chaine avec uniquement des lettres et des chiffres");
        return false;
    }
    return true;
}
</script>

</p>
</body>
</html>
'''


cherrypy.quickstart(VulnerableApp())
