# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: SYLLEBRANQUE, Pierre, pierre.syllebranque.etu@univ-lille.fr

- Nom, Prénom, email: FERNANDES, Antoine, 

## Question 1

Réponse : Le processus ne peut pas écrire car il est lancé par Toto et Toto ne peut pas écrire.

## Question 2

Réponse : 1. Pour un répertoire : 'x' signifie l'accessibilité au répertoire.
          2. Cela ne fonctionne pas, l'utilisateur toto étant dans le groupe ubuntu, il ne peut pas éxécuter le répertoire.
          3. Cela fonctionne car la lecture du répertoire est autorisé pour ubuntu.

## Question 3

Réponse : - id : 
* **EUID :** 1001
* **EGID :** 1001
* **RUID :** 1001
* **RGID :** 1001
Le processus n'arrive pas à ouvrir le fichier en lecture car le processus est lancé par toto qui n'a pas le droit d'ouvrir le repertoire `mydir`, donc ne peut pas lire `mydata.txt`.
          
          - id :
* **EUID :** 1000
* **EGID :** 1001
* **RUID :** 1001
* **RGID :** 1001 
Le processus arrive à ouvrir le fichier en lecture car maintenant que le flag est activé, le processus peut accéder au fichier.

## Question 4

Réponse : Quand le script est lancé par toto, les valeurs des ids sont :

* **EUID :** 1001
* **EGID :** 1001

## Question 5

Réponse : La commande `chnf` permet de modifier les informations contenu dans le fichier `/etc/passwd`.
Il y a différentes syntaxes possibles :

chfn [options] nom_utilisateur

Les Options :

- f => Nom complet
- r => N° du bureau
- w => Téléphone du bureau
- h => Téléphone personnel
- o => Autres informations

La commande : `ls -al /usr/bin/chfn` possède ces droits :

`-rwsr-xr-x`

- "-" -> c'est un fichier executable
- rws -> le ***user*** possède les droits suivant : lecture, écriture, exécution, le s indique la permission d'exécution avec setuid, ce qui engendre que tous les utilisateurs capable d'executer le fichier, l'executerons avec les droits du propriétaire.  
- r-x -> le ***group*** possède les droits suivant : lecture, exécution  
- r-x -> le ***other*** possède les droits suivant : lecture, exécution

!!faire le test avec chfn!!  

## Question 6

Réponse : Les mots de passe sont stockés dans un autre fichier `/etc/shadow`.
Car cela permet au système d'utiliser une méthode de "cryptage" ou "hash" pour crypter les mots de passe.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








