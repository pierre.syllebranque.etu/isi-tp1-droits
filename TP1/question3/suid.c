#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>



int main(int argc, char *argv[])
{
   
  printf("CHILD  %d -------------------\n", getpid()); 
  printf("     UID           GID  \n"
        "Real      %d  Real      %d  \n"
        "Effective %d  Effective %d  \n",
             getuid (),     getgid (),
             geteuid(),     getegid()
    );
  puts("---------------------------------");
 


  FILE * fPtr;

    char ch;
    fPtr = fopen(argv[1], "r");
    if(fPtr == NULL)
    {
        printf("Unable to open file.\n");
        printf("Please check whether file exists and you have read privilege.\n");
        exit(EXIT_FAILURE);
    }
    printf("File opened successfully. Reading file contents character by character. \n\n");

    do 
    {
        ch = fgetc(fPtr);
        putchar(ch);

    } while(ch != EOF); 

    fclose(fPtr);

    printf("\n" );

 

    return 0;
}
