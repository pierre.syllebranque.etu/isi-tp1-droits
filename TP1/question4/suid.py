import os

euid = os.geteuid();
egid = os.getgid();

print("identifiant de l'utilisateur effectif du processus actuel:", euid)
print("identifiant de groupe réel du processus actuel:", egid);